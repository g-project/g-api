from sklearn.metrics import mean_squared_error
import math
import time
from pyswarm import pso
from collections import defaultdict
from decimal import Decimal
from math import *
from sklearn import preprocessing
import pandas as pd
import os
import numpy as np
from sklearn.neighbors import KNeighborsClassifier
import warnings
import threading
import logging as logger
import json
from sklearn import preprocessing
from sklearn import utils

warnings.filterwarnings("ignore")

data_index_path = "src/data/dataset_index.csv"
pso_index_path = "src/data/pso.csv"
xopt_index_path = "src/data/xopt.npy"
recommend_path = "src/data/recommend.csv"


def create_dataindex():
    df = pd.read_csv("src/data/travel_feedback.csv")
    df = df.replace(-1.0, np.nan)
    df = df.dropna(how="any")

    le_location_city = preprocessing.LabelEncoder()
    le_user_city = preprocessing.LabelEncoder()
    le_partner_id = preprocessing.LabelEncoder()
    le_season_id = preprocessing.LabelEncoder()

    le_location_city.fit(df['location_city'].unique().tolist())
    le_user_city.fit(df['user_city'].unique().tolist())
    le_partner_id.fit(df['partner_id'].unique().tolist())
    le_season_id.fit(df['season_id'].unique().tolist())

    df['location_city'] = le_location_city.transform(df.location_city)
    df['user_city'] = le_user_city.transform(df.user_city)
    df['partner_id'] = le_partner_id.transform(df.partner_id)
    df['season_id'] = le_season_id.transform(df.season_id)

    columns = df.columns.tolist()
    columns.remove("score")
    columns.append("score")
    df = df[columns]

    df_new = df

    new_size_of_feature = defaultdict(dict)
    for column in df_new.columns:
        new_size_of_feature[column]['length'] = len(df_new[column].unique())
        new_size_of_feature[column]['names'] = df_new[column].unique()

    sum_of_features = 0
    for k, v in new_size_of_feature.items():
        sum_of_features += v['length']
    sum_of_features

    entriesToRemove = ('user_id', 'location_id')

    for k in entriesToRemove:
        new_size_of_feature.pop(k, None)

    # Index limits for setting Constraints
    index_limit = []
    for x in df_new.columns:
        index_limit.append(new_size_of_feature[x])

    index_limit = index_limit[2:-1]
    final_index_limit = []
    for i in range(len(index_limit)):
        x = index_limit[i]
        try:
            final_index_limit.append(x['length'])
        except Exception:
            pass

    final_index_limit, sum(final_index_limit)

    # Get Dummy Variables
    for column in df_new.columns[2:-1]:
        df_col = pd.get_dummies(df_new[column], prefix=column)
        df_new = pd.concat([df_new, df_col], axis=1)

    df_new = df_new.drop(['location_city', 'user_city', 'partner_id',
                          'season_id'], axis=1)

    columns = df_new.columns.tolist()
    columns.remove("score")
    columns.append("score")
    df_new = df_new[columns]

    with open(data_index_path, "w") as f:
        f.write(df_new.to_csv(index=False))


CNT = 0


def update_xopt():
    df_new = pd.read_csv(data_index_path)
    start_time = time.time()
    user_rating = df_new.score.tolist()

    def banana(x):
        global CNT
        CNT += 1
        if CNT % 10 == 0:
            print('.')
        if CNT % 100 == 0:
            print('|', CNT)
        lhs = 0.0
        square_error = 0.0
        for i in range(30):
            user_context = df_new.iloc[i][2:-1].tolist()
            lhs = np.dot(x, user_context)
            square_error += (user_rating[i] - lhs)**2
        return square_error/30

    def con(x):
        con_index = []
        final_index_limit = [30, 30, 4, 4]
        start_index = 0
        con_index = []
        for index in final_index_limit:
            temp = []
            temp.append(sum(x[start_index:start_index+index]) - 1)
            temp = temp*index
            con_index = con_index + temp
            start_index = index
        return con_index

    lb = [0]*68
    ub = [1]*68

    print('Run: ')
    xopt, fopt = pso(banana, lb, ub, f_ieqcons=con, swarmsize=50, maxiter=200)
    print("--- %s seconds ---" % (time.time() - start_time))

    original_rating = [df_new.iloc[i][-1] for i in range(df_new.shape[0])]
    predicted_rating = [np.dot(xopt, df_new.iloc[i][2:-1].tolist())
                        for i in range(df_new.shape[0])]
    print("----------------------  RMSE VALUE IS  ---------------------")
    mean_squared_error(original_rating, predicted_rating)
    np.save(xopt_index_path, xopt)

    # Save Pso result to csv
    def calculate_mse(weights):
        original_rating = [df_new.iloc[i][-1] for i in range(1000)]
        predicted_rating = [
            np.dot(weights, df_new.iloc[i][2:-1].tolist()) for i in range(1000)
        ]
        return mean_squared_error(original_rating, predicted_rating)

    def run_pso_multiple_times():
        swarm_size = 50
        indi_results = []
        for max_iter in [50, 100, 150, 200, 250, 300]:
            print("Current number of Iteration", str(max_iter))
            start_time = time.time()
            xopt, fopt = pso(banana, lb, ub, f_ieqcons=con,
                             swarmsize=swarm_size, maxiter=max_iter)
            mse = calculate_mse(xopt)
            finish_time = time.time() - start_time
            pso_param = [swarm_size, max_iter, finish_time, mse]
            indi_results.append(pso_param + xopt.tolist())
        indi_results = np.array(indi_results)
        np.savetxt("src/data/pso.csv", indi_results, delimiter=",")

    run_pso_multiple_times()


if os.path.exists(data_index_path):
    df_new = pd.read_csv(data_index_path)
    xopt = np.load(xopt_index_path)
    xopt = np.array(xopt)
    weights = xopt
    columns = df_new.columns
    X = df_new[columns[2:-1]]
    y = df_new[columns[-1:]]

    location = df_new.drop_duplicates(
        ['location_id'], keep='first').reset_index(drop=True)
    location = location['location_id']

    user = df_new.drop_duplicates(
        ['user_id'], keep='first').reset_index(drop=True)


class neighbors:
    def __init__(self, X, y):
        self.model = KNeighborsClassifier(n_neighbors=10)
        self.model.fit(X, y)

    def get_neighbors_data(self, user, location_id):
        neighbors_data = df_new[(df_new.location_id == location_id)]
        self.good_neighbors = pd.DataFrame()
        threshold = 0.0
        for index, row in neighbors_data.iterrows():
            try:
                similarity = jaccard_similarity_score(row[2:-1], user[2:-1])
            except Exception as e:
                print('index: ', index)
                print('x: ', row[2:-1])
                print('y: ', user[2:-1])
                raise e
            if similarity > threshold:
                self.good_neighbors = self.good_neighbors.append(
                    row, ignore_index=True)
        self.good_neighbors = self.good_neighbors[df_new.columns]
        return self.good_neighbors


class neighborhood_contribution:
    def __init__(self):
        pass

    def neighbor_rating(self, neighbor, location_id, sigma, threshold):
        self.ratings_sum = 0
        self.similarity_sum = 0
        threshold = 0
        ratings = df_new[(df_new.user_id == neighbor.user_id)
                         & (df_new.location_id == location_id)]
        for index, user_rating in ratings.iterrows():
            similarity = jaccard_similarity_score(
                neighbor[2:-1], user_rating[2:-1])
            if similarity > threshold:
                self.ratings_sum += user_rating.score * similarity
                self.similarity_sum += similarity
        try:
            rating = self.ratings_sum / self.similarity_sum
        except Exception:
            rating = 0
        return rating

    def neighbor_average(self, neighbor, sigma, threshold):
        ratings_of_neighbor_mean = df_new[df_new.user_id ==
                                          neighbor.user_id].score.mean()
        return ratings_of_neighbor_mean

    def baseline_rating(self, location_id):
        average_item_rating = df_new[
            df_new.location_id == location_id].score.mean()
        return average_item_rating


def predict_rating(user, itemID):
    user_neighbors = neighbors(X, y)
    user_neighborhood_contribution = neighborhood_contribution()
    neighbors_data = user_neighbors.get_neighbors_data(user, itemID)
    similarity_of_neighbors = []
    neighborhood_contribution_rating = []
    for index, neighbor in neighbors_data.iterrows():
        similarity_of_neighbors.append(
            jaccard_similarity_score(neighbor[2:-1], user[2:-1]))

        neighborhood_contribution_rating.append(user_neighborhood_contribution.neighbor_rating(
            neighbor, itemID, 1, 0.1) - user_neighborhood_contribution.neighbor_average(neighbor, 1, 0.1))

    numerator_value = numerator(
        neighborhood_contribution_rating, similarity_of_neighbors)

    baseline_rating = user_neighborhood_contribution.baseline_rating(itemID)

    if sum(similarity_of_neighbors) == 0:
        return baseline_rating
    final_rating = baseline_rating + \
        numerator_value/sum(similarity_of_neighbors)
    return final_rating


def numerator(neighborhood_contribution_rating, similarity_of_neighbors):
    numerator_sum = 0
    for neighbor_rating, similarity in zip(neighborhood_contribution_rating,
                                           similarity_of_neighbors):
        numerator_sum += neighbor_rating*similarity
    return numerator_sum


def jaccard_similarity_score(x, y):
    intersection = np.logical_and(np.array(x), np.array(y)).astype('int')
    union = np.logical_or(np.array(x), np.array(y)).astype('int')
    numerator = np.dot(np.array(weights), intersection)
    denom = np.dot(np.array(weights), union)
    similarity = numerator/denom
    if similarity == 0:
        return 0
    return round(similarity, 2)


def create_recommend(userSearch):
    df_new = pd.read_csv(data_index_path)
    location = df_new.drop_duplicates(
        ['location_id'], keep='first').reset_index(drop=True)
    location = location['location_id']

    user = df_new.drop_duplicates(
        ['user_id'], keep='first').reset_index(drop=True)

    userId = user[(user.user_id == userSearch)]
    start = time.time()
    for i in range(len(userId)):
        data_Array = []
        for k in range(len(location)):
            temp2 = k
            rating = predict_rating(userId.iloc[i], location[temp2])
            print("========>Loading.....%f......", (time.time() - start))
            if (rating > 3 and rating <= 5):
                data_Array.append({
                    'user_id': userSearch,
                    'val': rating,
                    'location_id': location[temp2]
                })
        file = "src/data/recommend/"+userSearch+".csv"
        print(file)
        try:
            with open(file, 'w') as fp:
                json.dump(data_Array, fp)
        except Exception as e:
            print("Error %r", e)
        print("OK")


def predict_rating_async(user):
    print("predict_rating_async")
    job = threading.Thread(target=create_recommend,
                           args=(user, ), daemon=True)
    job.start()


if __name__ == '__main__':
    # print(location)
    # predict_rating_async("5dcc26f2212532d2efeb1456")
    create_recommend("5dcc26f2212532d2efeb1456")
    # update_xopt()
    # logger.warning("OK")
    # start = time.time()
    # df_new = pd.read_csv(data_index_path)
    # rating = predict_rating(df_new.iloc[59], df_new.iloc[61][1])
    # print(rating)
    # create_dataindex()
    # logger.warning("%f--- seconds ---", rating)
