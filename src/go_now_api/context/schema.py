Partner = {
    'item_title': 'partner',
    'public_methods': ['GET', 'POST'],
    'schema': {
        'name': {
            'type': 'string',
            'unique': True
        }
    }
}

Season = {
    'item_title': 'season',
    'public_methods': ['GET', 'POST'],
    'schema': {
        'name': {
            'type': 'string',
            'unique': True
        }
    }
}
