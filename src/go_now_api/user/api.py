import logging as logger
from flask import jsonify, request, session
from eve.render import send_response
from eve.methods.get import getitem_internal
from eve.methods.post import post_internal
from bson import ObjectId
import json
from src.Travel_Recsys_new import predict_rating_async
import pandas as pd
from src.mongo_to_csv import read_mongo
import random
import os

if os.path.exists("src/data/dataset_index.csv"):
    df_new = pd.read_csv("src/data/dataset_index.csv")
    location = df_new.drop_duplicates(
        ['location_id'], keep='first').reset_index(drop=True)
    location = location['location_id']
    user = df_new.drop_duplicates(
        ['user_id'], keep='first').reset_index(drop=True)
    with open("src/data/recommend.csv") as json_file:
        data = json.load(json_file)
        array = []
        for p in range(len(location)):
            temp = 0
            for i in data:
                if(i['location_id'] == location[p]):
                    temp += i['val']
            array.append({'val': (temp/len(user)),
                          'location_id': location[p]})
        array = sorted(array, key=lambda x: x['val'], reverse=True)
        trend_array = []
        for i in range(10):
            trend_array.append(array[i])


def setup(app):
    @app.route('/login', methods=['POST'])
    def login():
        payload = request.json
        user = app.data.find_one(
            'user', None, None, username=payload['username'],
            password=payload['password'])

        if user:
            session['user_id'] = str(user['_id'])
            file = "src/data/recommend/"+str(user['_id'])+".csv"
            logger.warning(file)
            with open(file) as json_file:
                recommend_data = json.load(json_file)
            recommend_data = sorted(recommend_data, key=lambda x: x['val'], reverse=True)
            recommend = []
            for p in recommend_data:
                if(len(recommend) < 10):
                    recommend.append(p)
            recommend = sorted(recommend, key=lambda x: x['val'], reverse=True)
            return jsonify({
                'status': 'OK',
                'username': user['username'],
                'id': str(user['_id']),
                'email': user['email'],
                'user_city': user['address'],
                'recommend': recommend
            })
        return jsonify({
            'status': 'Error',
        }), 401

    @app.route('/get_trend', methods=['GET'])
    def get_trend():
        return jsonify({
            'status': 'ok',
            'recommend': trend_array
        })

    @app.route('/register', methods=['POST'])
    def register():
        payload = request.json
        del payload['confirm']
        response, _, _, return_code, location_header = post_internal(
            'user', payload)
        df = read_mongo('go-now-db', 'feedback', {},
                        host='localhost', port=27017)
        df.to_csv('src/data/travel_feedback.csv', index=False)
        user = app.data.find_one(
            'user', None, None, username=payload['username'],
            password=payload['password'])
        if user:
            session['user_id'] = str(user['_id'])
            return jsonify({
                'status': 'OK',
                'username': user['username'],
                'id': str(user['_id']),
                'email': user['email'],
                'user_city': user['address'],
            })
        return jsonify({
            'status': 'Error',
        }), 401

    @app.route('/user/info', methods=['GET'])
    def user_info():
        user_id = session.get('user_id')
        response, last_modified, etag, status = getitem_internal(
            'user', _id=ObjectId(user_id))
        return send_response('user', (response, last_modified,
                                      etag, status))
