User = {
    'public_methods': ['GET', 'POST'],
    'public_item_methods': ['GET', 'POST'],
    'item_methods': ['GET', 'PATCH'],
    'schema': {
        'username': {
            'type': 'string',
            'minlength': 1,
            'maxlength': 15,
            'unique': True,
            'required': True
        },
        'password': {
            'type': 'string',
            'required': True
        },
        'firstname': {
            'type': 'string',
            'minlength': 1,
            'maxlength': 15,
        },
        'lastname': {
            'type': 'string',
            'minlength': 1,
            'maxlength': 15,
        },
        'gender': {
            'type': 'string',
        },
        'birthday': {
            'type': 'string',
            # 'coerce': to_date
        },
        'email': {
            'type': 'string',
            'unique': True,
        },
        'phone': {
            'type': 'string',
        },
        'address': {
            'type': 'string',
        },
        'age': {
            'type': 'integer'
        },
        'telephone': {
            'type': 'integer'
        }
    }
}

Profile = {
    'item_title': 'profile',

    'resource_methods': ['GET', 'POST'],
    'item_methods': ['GET', 'PATCH'],

    'datasource': {
        'source': 'user'
    },

    'schema': {
        'username': {
            'type': 'string',
            'minlength': 1,
            'maxlength': 15,
            'unique': True
        },
        'firstname': {
            'type': 'string',
            'minlength': 1,
            'maxlength': 15,
        },
        'lastname': {
            'type': 'string',
            'minlength': 1,
            'maxlength': 15,
            'required': True,
        },
        'gender': {
            'type': 'string',
        },
        'birthday': {
            'type': 'string',
        },
        'email': {
            'type': 'string',
            'unique': True,
        },
        'phone': {
            'type': 'string',
        },
        'address': {
            'type': 'string',
        },
        'age': {
            'type': 'integer'
        },
        'telephone': {
            'type': 'integer'
        }
    }
}