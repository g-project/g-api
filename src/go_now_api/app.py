from flask import g, request, abort, session, current_app as app
from eve import Eve
from go_now_api.user import api as UserApi
from go_now_api.travel import api as TravelApi
from go_now_api.feedback import api as FeedbackApi
from go_now_api.find_location import api as FindLocationApi
from bson import ObjectId


class BasicAuth(object):
    def set_mongo_prefix(self, value):
        g.mongo_prefix = value

    def get_mongo_prefix(self):
        return g.get("mongo_prefix")

    def set_request_auth_value(self, value):
        g.auth_value = value

    def get_request_auth_value(self):
        return g.get("auth_value")

    def get_user_or_token(self):
        return g.get("user")

    def set_user_or_token(self, user):
        g.user = user

    def check_auth(self, username, password, allowed_roles, resource, method):
        raise NotImplementedError

    def authenticate(self):
        """ Returns a standard a 401 response that enables basic auth.
        Override if you want to change the response and/or the realm.
        """
        abort(
            401,
            "Please provide proper credentials",
            www_authenticate=("WWW-Authenticate",
                              'Basic realm="%s"' % __package__),
        )

    def authorized(self, allowed_roles, resource, method):
        """ Validates the the current request is allowed to pass through.

        :param allowed_roles: allowed roles for the current request, can be a
                              string or a list of roles.
        :param resource: resource being requested.
        """
        auth = request.authorization
        if auth:
            self.set_user_or_token(auth.username)
        return auth and self.check_auth(
            auth.username, auth.password, allowed_roles, resource, method
        )


class BCryptAuth(BasicAuth):
    def check_auth(self, userId, allowed_roles, resource, method):
        # use Eve's own db driver; no additional connections/resources are used
        users = app.data.driver.db['user']
        user = users.find_one({'_id': ObjectId(userId)})
        return user

    def authorized(self, allowed_roles, resource, method):
        userId = session.get('user_id')

        if userId:
            self.set_user_or_token(userId) 
        return userId and self.check_auth(userId, allowed_roles, resource,
                                          method)


application = Eve(auth=BCryptAuth)


@application.after_request
def cors_with_credential(response):
    response.headers['Access-Control-Allow-Credentials'] = 'true'
    response.headers.add("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PATCH, PUT, DELETE")
    response.headers['Access-Control-Allow-Origin'] = 'http://localhost:3000'
    response.headers['Access-Control-Allow-Headers'] = ",".join(
        ['Content-Type', 'If-Match'])
    return response


TravelApi.setup(application)
UserApi.setup(application)
FeedbackApi.setup(application)
FindLocationApi.setup(application)
