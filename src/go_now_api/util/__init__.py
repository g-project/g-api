import logging as logger


def to_date(s):
    from datetime import datetime
    return datetime.strptime(s, '%Y-%m-%d')
