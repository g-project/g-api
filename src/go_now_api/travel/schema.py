City = {
    'item_title': 'city',
    'public_methods': ['GET', 'POST'],
    'mongo_indexes': {'text': ([('title', "text")])},
    'schema': {
        'title': {
            'type': 'string',
            'required': True,
            'unique': True,
        }
    }
}

Location = {
    'item_title': 'location',
    'public_methods': ['POST', 'GET'],
    'public_item_methods': ['GET', 'PATCH'],
    'item_methods': ['GET', 'PATCH'],
    'mongo_indexes': {'text': ([('title', "text")])},
    'schema': {
        'city_id': {
            'type': 'objectid',
            'data_relation': {
                'resource': 'city',
                'field': '_id',
                'embeddable': True
            },
        },
        'title': {
            'type': 'string',
            'required': True,
            'unique': True,
        },
        'description': {
            'type': 'string',
        },
        'map_location': {
            'type': 'string',
        },
        'address': {
            'type': 'string',
        },
        'image': {
            'type': 'string',
        },
        'type': {
            'type': 'number',
        },
        'rate': {
            'type': 'integer',
        },
    }

}
