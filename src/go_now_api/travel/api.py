import logging as logger
from flask import request, jsonify
from eve.render import send_response
from eve.methods.post import post_internal
from eve.methods.get import getitem_internal


def setup(app):
    pass
    @app.route('/point', methods=['POST'])
    def post_city():
        logger.warning("City here %r", app.data)
        payload = request.json
        city = app.data.find_one(
            'city', None, None, title=payload['title'])

        if city:
            return jsonify({
                'status': 'OK',
                'city_id': city['_id'].toString()
            })

        return jsonify({
            'status': 'Error',
        }), 401

    @app.route('/location', methods=['POST'])
    def post_point():
        logger.warning("Point here %r", app.data)
        payload = request.json
        response, _, _, return_code, location_header = post_internal(
            'point', payload)
        return send_response('city', (response, None, None,
                                      return_code,
                                      location_header))
