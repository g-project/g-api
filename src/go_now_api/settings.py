from go_now_api.user.schema import Profile, User
from go_now_api.travel.schema import City, Location
from go_now_api.context.schema import Partner, Season
from go_now_api.feedback.schema import Feedback, Feedback_saving, Invitation
from go_now_api.find_location.schema import Request_Recommend


MONGO_HOST = 'localhost'
MONGO_PORT = 27017

MONGO_DBNAME = 'go-now-db'

# RESOURCE_METHODS = ['GET', 'POST', 'DELETE']
# ITEM_METHODS = ['GET', 'PATCH', 'PUT', 'DELETE']
QUERY_MAX_RESULTS = 'max_results'

SECRET_KEY = "sdfjhsdkdjfahk"

EMBEDDING = True

DOMAIN = {
    'user': User,
    'profile': Profile,
    'city': City,
    'location': Location,
    'partner': Partner,
    'season': Season,
    'feedback': Feedback,
    'feedback_saving': Feedback_saving,
    'request': Request_Recommend,
    'invitation': Invitation
}


X_DOMAINS = ['http://localhost:3000']
X_HEADERS = ['Access-Control-Allow-Origin', 'Access-Control-Allow-Credentials']
X_EXPOSE_HEADERS = ['content-type']
X_ALLOW_CREDENTIALS = True
