Feedback = {
    'item_title': 'conmment',
    'public_methods': ['GET'],
    'public_item_methods': ['POST', 'GET', 'PATCH', 'DELETE'],
    'schema': {
        'location_id': {
            'type': 'objectid',
            'data_relation': {
                'resource': 'location',
                'field': '_id',
                'embeddable': True
            },
            'required': True
        },
        'user_id': {
            'type': 'objectid',
            'data_relation': {
                'resource': 'user',
                'field': '_id',
                'embeddable': True
            },
        },
        'location_city': {
            'type': 'objectid',
            'data_relation': {
                'resource': 'city',
                'field': '_id',
                'embeddable': True
            },
        },
        'user_city': {
            'type': 'string',
        },
        'partner_id': {
            'type': 'objectid',
            'data_relation': {
                'resource': 'partner',
                'field': '_id',
                'embeddable': True
            },
            'required': True
        },
        'season_id': {
            'type': 'objectid',
            'data_relation': {
                'resource': 'season',
                'field': '_id',
                'embeddable': True
            },
            'required': True
        },
        'comment': {
            'type': 'string',
        },
        'score': {
            'type': 'number',
            'required': True
        }
    }
}

Feedback_saving = {
    'item_title': 'conmment',
    'resource_methods': ['GET', 'POST'],
    'public_methods': ['GET', 'POST'],
    'item_methods': ['GET', 'PATCH', 'DELETE'],
    'public_item_methods': ['POST', 'GET', 'PATCH', 'DELETE'],
    'schema': {
        'user_id': {
            'type': 'objectid',
            'data_relation': {
                'resource': 'user',
                'field': '_id',
                'embeddable': True
            },
            'required': True
        },
        'location_id': {
            'type': 'objectid',
            'data_relation': {
                'resource': 'location',
                'field': '_id',
                'embeddable': True
            },
            'required': True
        },
        'location_city': {
            'type': 'objectid',
            'data_relation': {
                'resource': 'city',
                'field': '_id',
                'embeddable': True
            },
        },
        'user_city': {
            'type': 'string',
        },
        'partner_id': {
            'type': 'string',
        },
        'season_id': {
            'type': 'string',
        },
        'comment': {
            'type': 'string',
        },
        'score': {
            'type': 'number',
        },
        'invite': {
            'type': 'list',
            'schema': {
                'type': 'objectid',
                'data_relation': {
                    'resource': 'user',
                    'field': '_id',
                    'embeddable': True
                }
            }
        },
        'submit_invited': {
            'type': 'list',
            'schema': {
                'type': 'objectid',
                'data_relation': {
                    'resource': 'user',
                    'field': '_id',
                    'embeddable': True
                }
            }
        }
    }
}

Invitation = {
    'item_title': 'invitation',
    'resource_methods': ['GET', 'POST', 'DELETE'],
    'public_methods': ['GET', 'POST', 'DELETE'],
    'item_methods': ['GET', 'PATCH', 'DELETE'],
    'public_item_methods': ['POST', 'GET', 'PATCH', 'DELETE'],
    'schema': {
        'invitor': {
            'type': 'objectid'
        },
        'invitee': {
            'type': 'objectid'
        },
        'location_id': {
            'type': 'objectid'
        },
        'status': {
            'type': 'string'
        }
    }
}
