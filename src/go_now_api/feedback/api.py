import logging as logger
from flask import request, jsonify
from eve.render import send_response
from eve.methods.patch import patch_internal
from eve.methods.post import post_internal
from src.mongo_to_csv import read_mongo
from src.Travel_Recsys_new import create_dataindex
from src.Travel_Recsys_new import create_recommend
import pandas as pd
import threading


def setup(app):
    @app.route('/feedback', methods=['POST'])
    def post_feedback():
        recommend_path = "src/data/recommend.csv"
        data_index_path = "src/data/dataset_index.csv"
        payload = request.json
        response, _, _, return_code, location_header = post_internal(
            'feedback', payload)
        df = read_mongo('go-now-db', 'feedback', {},
                        host='localhost', port=27017)
        df = df[['user_id', 'location_id', 'score', 'location_city',
                 'user_city', 'partner_id', 'season_id']]
        df = df.astype({'score': 'int'})
        df.to_csv('src/data/travel_feedback.csv', index=False)
        create_dataindex()
        logger.warning(str(payload['user_id']))
        # predict_rating_async(str(payload['user_id']))
        job = threading.Thread(target=create_recommend,
                               args=(str(payload['user_id']), ), daemon=True)
        job.start()
        return jsonify({
            'status': 'OK'
        })

    @app.route('/invite_user', methods=['POST'])
    def get_user_invite():
        payload = request.json
        df = read_mongo('go-now-db', 'feedback_saving', {},
                        host='localhost', port=27017, no_id=False)
        df = df[['_id', 'user_id', 'location_id', '_etag']]
        df = df.astype({'location_id': 'str', 'user_id': 'str'})
        df = df[(df['location_id'] == payload['location_id']) & (df['user_id'] != payload['user_id'])]
        df = df[['_id', 'user_id', 'location_id', '_etag']]
        # df = df.astype({'_id': 'str'})
        df['str_id'] = df['_id'].apply(lambda x: str(x))
        df['_id'] = df['str_id']
        df = df.drop(columns=['str_id'])
        logger.warning("df type %r" % df.dtypes)
        return jsonify({
            'status': 'OK',
            'user': df.values.tolist(),
        })
