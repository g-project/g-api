Request_Recommend = {
    'item_title': 'request_recommend',
    'item_methods': ['DELETE'],
    'resource_methods': ['DELETE'],
    'public_methods': ['GET', 'DELETE'],
    'public_item_methods': ['GET', 'DELETE'],
    'schema': {
        'user_id': {
            'type': 'string',
        },
        'location_id': {
            'type': 'string',
        },
        'location_city': {
            'type': 'objectid',
            'data_relation': {
                'resource': 'city',
                'field': '_id',
                'embeddable': True
            },
        },
        'user_city': {
            'type': 'string',
        },
        'partner_id': {
            'type': 'string',
        },
        'season_id': {
            'type': 'string',
        },
        'score': {
            'type': 'number',
        }
    }
}