import logging as logger
from flask import request, jsonify
from eve.render import send_response
from eve.methods.patch import patch_internal
from eve.methods.post import post_internal
from src.mongo_to_csv import read_mongo
from src.Travel_Recsys_new import create_dataindex
from src.Travel_Recsys_new import predict_rating_async
from src.Travel_Recsys_new import predict_rating
import pandas as pd
import os
import json


def setup(app):
    @app.route('/del_fb', methods=['DELETE'])
    def del_travel_fb():
        if os.path.exists("src/data/travel_feedback.csv"):
            os.remove("src/data/travel_feedback.csv")
            logger.warning("========>Deleted travel_feedback...........")
            return jsonify({
                'status': 'ok'
            })

    @app.route('/request', methods=['POST'])
    def post_request():
        payload = request.json
        response, _, _, return_code, location_header = post_internal(
            'request', payload)

        df = read_mongo('go-now-db', 'feedback', {},
                        host='localhost', port=27017)
        df = df[['user_id', 'location_id', 'score', 'location_city',
                 'user_city', 'partner_id', 'season_id']]

        df.to_csv("src/data/travel_feedback.csv", index=False)
        return send_response('request', (response, None, None,
                                         return_code, location_header))

    @app.route('/recommend', methods=['GET'])
    def get_recommend():
        recommend_path = "src/data/travel_feedback.csv"
        data_index_path = "src/data/dataset_index.csv"

        df = read_mongo('go-now-db', 'request', {},
                        host='localhost', port=27017)
        logger.warning("========>Deleted %r...........", df)
        df = df[['user_id', 'location_id', 'score', 'location_city',
                 'user_city', 'partner_id', 'season_id']]

        df.to_csv(recommend_path, index=False, mode='a+', header=False)
        create_dataindex()
        df_new = pd.read_csv(data_index_path)
        location = df['location_id']
        recommend = []
        for i in range(len(location)):
            rating = predict_rating(df_new.iloc[-1], location[i])
            recommend.append({'val': rating,
                              'location_id': location[i]})
            logger.warning("========>Loading.....%f......", rating)
        return jsonify({
            'status': 'ok',
            'recommend': recommend
        })

    @app.route('/recommend', methods=['POST'])
    def update_recommend():
        payload = request.json
        file = "src/data/recommend/"+payload['id']+".csv"
        logger.warning(file)
        with open(file) as json_file:
            recommend_data = json.load(json_file)
        recommend_data = sorted(
            recommend_data, key=lambda x: x['val'], reverse=True)
        recommend = []
        for p in recommend_data:
            if(len(recommend) < 12):
                recommend.append(p)
        recommend = sorted(recommend, key=lambda x: x['val'], reverse=True)
        return jsonify({
            'status': 'OK',
            'recommend': recommend
        })
