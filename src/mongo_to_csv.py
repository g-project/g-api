import pandas as pd
from pymongo import MongoClient


def _connect_mongo(host, port, username, password, db):
    """ A util for making a connection to mongo """

    if username and password:
        mongo_uri = 'mongodb://%s:%s@%s:%s/%s' % (username, password,
                                                  host, port, db)
        conn = MongoClient(mongo_uri)
    else:
        conn = MongoClient(host, port)
    return conn[db]


def read_mongo(db, collection, query={}, host='localhost', port=27017,
               username=None, password=None, no_id=True):
    # Connect to MongoDB
    db = _connect_mongo(host=host, port=port, username=username,
                        password=password, db=db)

    # Make a query to the specific DB and Collection
    cursor = db[collection].find(query)

    # Expand the cursor and construct the DataFrame
    df = pd.DataFrame(list(cursor))

    # Delete the _id
    if no_id and '_id' in df:
        del df['_id']
        del df['_updated']
        del df['_created']
        del df['_etag']

    return df


if __name__ == '__main__':
    df = read_mongo('go-now-db', 'location', {},
                    host='localhost', port=27017, no_id=False)
    df = df[['_id', 'city_id', 'title',
             'address', 'image']]
    df.to_csv("src/data/location_infor.csv", index=False)
